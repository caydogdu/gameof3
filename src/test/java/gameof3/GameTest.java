package gameof3;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.takeaway.gameof3.game.Game;
import com.takeaway.gameof3.model.MoveRequest;


@RunWith(SpringRunner.class)
public class GameTest {

	@Test
	public void addlayer() throws Exception {

		Game game = new Game(100, 1000);
		
		game.addPlayer();
		assertEquals(1, game.getPlayerCount());
		
		game.addPlayer();
		assertEquals(2, game.getPlayerCount());
		
	}
	
	@Test
	public void startGame() throws Exception {
		
		Game game = new Game(100, 1000);
		
		game.addPlayer();
		game.addPlayer();

		MoveRequest moveRequest = new MoveRequest();
		moveRequest.setPlayerIndex(0);
		game.startGame(moveRequest);
		assertEquals(true, game.isGameStarted());

	}
	
	@Test
	public void playGame() throws Exception {
		
		Game game = new Game(100, 1000);
		
		game.addPlayer();
		game.addPlayer();

		MoveRequest moveRequest = new MoveRequest();
		moveRequest.setPlayerIndex(0);
		game.startGame(moveRequest);

		boolean gameOver = false;
		while(!game.isGameOver()){
			int adj = game.getAdjustmentToDivide3(game.move.getNumber());
			MoveRequest mr = new MoveRequest();
			mr.setAdjustment(String.valueOf(adj));
			mr.setPlayerIndex(game.move.getTurn().getPlayer().getIndex());
			game.playGame(mr);
			if(game.isGameOver()){
				gameOver = true;
			}
		}
		
		assertEquals(true, gameOver);

	}

}

package com.takeaway.gameof3.model;

public class Turn {

	private Player player;

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void switchPlayer(){	
		player.setIndex((player.getIndex() + 1)%2);
	}
	
}

package com.takeaway.gameof3.model;

public class MoveRequest {

	private int playerIndex;
    
    private String playerName;
    
    private String adjustment;
    
    private int number;

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getAdjustment() {
		return adjustment;
	}

	public void setAdjustment(String adjustment) {
		this.adjustment = adjustment;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "MoveRequest [playerIndex=" + playerIndex + ", playerName="
				+ playerName + ", adjustment=" + adjustment + ", number="
				+ number + "]";
	}
    
}

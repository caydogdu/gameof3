package com.takeaway.gameof3.model;

public class Message {

	private int playerIndex;
	
    private String content;
    
    public Message(){
    }
    
    public Message(String content){
    	this.content = content;
    }
    
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Message [playerIndex=" + playerIndex + ", content=" + content
				+ "]";
	}
	
}

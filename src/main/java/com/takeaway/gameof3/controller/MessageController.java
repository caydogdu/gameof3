package com.takeaway.gameof3.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.takeaway.gameof3.game.Game;
import com.takeaway.gameof3.model.Message;
import com.takeaway.gameof3.model.MoveRequest;

@Controller
public class MessageController {

	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	
	private Game game = null;
	
	@Value("${number.min}")
    private int min;
	
	@Value("${number.max}")
    private int max;
	
	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	@MessageMapping("/join")
    public void join() throws Exception {
		
		// if game is null create a game
        if(game == null){
        	game = new Game(min,max);
        }
        
        // add player and send message
        Message message = game.addPlayer();
        logger.info(message.toString());
        messagingTemplate.convertAndSend("/topic/messages", message);
        
    }
	
	@MessageMapping("/start")
    public void start(MoveRequest moveRequest) throws Exception {
		logger.info(moveRequest.toString());
		// start game and send message
		if(game.getPlayerCount() < 2){
    		messagingTemplate.convertAndSend("/topic/messages", new Message("waiting for another player to start game"));
    		return;
    	} 
		messagingTemplate.convertAndSend("/topic/messages", game.startGame(moveRequest));
    }
	
    @MessageMapping("/play")
    public void play(MoveRequest moveRequest) throws Exception {
        logger.info(moveRequest.toString());
		if(game.isGameOver()){
			messagingTemplate.convertAndSend("/topic/messages", new Message("game is over"));
    		return;
		}
		if(!game.isGameStarted()){
			messagingTemplate.convertAndSend("/topic/messages", new Message("waiting for another player to start game"));
    		return;
    	} 
        messagingTemplate.convertAndSend("/topic/messages", game.playGame(moveRequest));
    }

}

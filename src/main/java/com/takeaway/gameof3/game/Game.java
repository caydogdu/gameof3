package com.takeaway.gameof3.game;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.takeaway.gameof3.controller.MessageController;
import com.takeaway.gameof3.model.Message;
import com.takeaway.gameof3.model.Move;
import com.takeaway.gameof3.model.MoveRequest;
import com.takeaway.gameof3.model.Player;
import com.takeaway.gameof3.model.Turn;

public class Game extends AbstractGame {

	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	
	private int[] adjustmentArray = {-1,0,1};

	public Game(int numberMin, int numberMax) {
		super(numberMin, numberMax);
	}

	/**
	 * a player is added to game
	 * @return message for players
	 */
	@Override
	public Message addPlayer() {
		
		Message message = new Message();
		Player player = null;
    	
    	if(getPlayerCount() < 2){
    		player = new Player(getPlayerCount(),"Player " + getPlayerCount());
    		getPlayers().add(player);
    		message.setPlayerIndex(player.getIndex());
    		message.setContent(player.getName() + " is ready to start");
    	}
    	
    	return message;
		
	}
	
	/**
	 * a player starts game and send a number to other player
	 * @param moveRequest
	 * @return message for players
	 */
	@Override
	public Message startGame(MoveRequest moveRequest) {
		
		Message message = new Message();
				
		Player player = getPlayers().get(moveRequest.getPlayerIndex());
		
		// set a number for game
		move = new Move();
		Random random = new Random();
		move.setNumber( getNumberMin() + random.nextInt(getNumberMax()));
		
		// switch player
		Turn turn = new Turn();
		turn.setPlayer(player);
		turn.switchPlayer();
		move.setTurn(turn);
		
		message.setContent(player.getName() + " started a game and send " + move.getNumber());
		
		return message;
		
	}
	
	/**
	 * A player send adjustment, new number is calculated and send to other player
	 * @param moveRequest
	 * @return message for players
	 */
	@Override
	public Message playGame(MoveRequest moveRequest) {
		
		Message message = new Message();
    	
		int turn = getMove().getTurn().getPlayer().getIndex();
		logger.info("move number : " + getMove().getNumber() + " move turn : " + turn );
    	logger.info("player : " + moveRequest.getPlayerIndex() + " adjustment : " + moveRequest.getAdjustment());

    	// wrong turn
    	if(turn != moveRequest.getPlayerIndex()){
    		message.setContent("oops " + getPlayers().get(turn).getName() + " is on turn");
    		return message;
    	}
    	
    	int adj = 0;
    	if(moveRequest.getAdjustment().equals("")){
    		// if player did not typed a input play automatically
    		adj = getAdjustmentToDivide3(getMove().getNumber());
    	} else{
    		adj = Integer.parseInt(moveRequest.getAdjustment());
    	}

    	boolean divisible = ((getMove().getNumber() + adj)%3) == 0 ? true : false;
    	// if adjustment different then -1,0,1 or it does not allow to divide throw exception
		if(!adjustmentArrayContains(adj) || !divisible){
			message.setContent("oops invalid adjustment");
			return message;
		}
    	
		//calculate new number and switch player
		getMove().setNumber((getMove().getNumber() + adj)/3);
		getMove().getTurn().switchPlayer();
    	
    	// game over
    	if(getMove().getNumber() == 1){
    		message.setContent(getPlayers().get(moveRequest.getPlayerIndex()).getName() + " won");
    		setGameOver(true);
    		return message;
    	}
    	
    	message.setContent(getPlayers().get(moveRequest.getPlayerIndex()).getName() + 
    			" added " + adj + " and send " + move.getNumber());
    	
    	return message;
		
	}
	
	/**
	 * 
	 * @param number
	 * @return adjustment value to divide number to 3
	 */
	public int getAdjustmentToDivide3(int number){
		int sum = sumOfDigits(number);
		for(int i=-1; i<2; i++){
			if((sum + i)%3 == 0)
				return i;
		}
		return 0;
	}
	
	/**
	 * 
	 * @param number
	 * @return sum of digits for given number
	 */
	private int sumOfDigits(int number){
		
		if(number < 10)
			return number;
		
		int sum = 0;
		while (number > 9) {
			sum = 0;
			while (number > 0) {
				int rem;
				rem = number % 10;
				sum = sum + rem;
				number = number / 10;
			}
			number = sum;
		}
		return sum;
		
	}
	
    /**
     * 
     * @param adjustment
     * @return if adjustment exits in adjustmentArray
     */
	private boolean adjustmentArrayContains(int adjustment) {
		for(int i=0; i<adjustmentArray.length; i++){
			if(adjustmentArray[i] == adjustment){
				return true;
			}
		}
		return false;
	}

}

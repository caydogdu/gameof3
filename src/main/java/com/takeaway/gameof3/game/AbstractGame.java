package com.takeaway.gameof3.game;

import java.util.ArrayList;
import java.util.List;

import com.takeaway.gameof3.model.Message;
import com.takeaway.gameof3.model.Move;
import com.takeaway.gameof3.model.MoveRequest;
import com.takeaway.gameof3.model.Player;

public abstract class AbstractGame {

	private List<Player> players = new ArrayList<Player>();
	
	public Move move;
	
	private int numberMin;
	
	private int numberMax;
	
	private boolean gameOver;
	
	public AbstractGame(int numberMin, int numberMax){
		this.numberMin = numberMin;
		this.numberMax = numberMax;
	}
	
	public abstract Message addPlayer();
	
	public abstract Message startGame(MoveRequest moveRequest);
	
	public abstract Message playGame(MoveRequest moveRequest);
	
	public int getPlayerCount() {
		return getPlayers().size();
	}

	public boolean isGameStarted() {
		return getMove() == null ? false : true;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public Move getMove() {
		return move;
	}

	public void setMove(Move move) {
		this.move = move;
	}

	public int getNumberMin() {
		return numberMin;
	}

	public void setNumberMin(int numberMin) {
		this.numberMin = numberMin;
	}

	public int getNumberMax() {
		return numberMax;
	}

	public void setNumberMax(int numberMax) {
		this.numberMax = numberMax;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
	
}

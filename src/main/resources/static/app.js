var stompClient = null;
var playerName = null;
var playerIndex = null;

function setConnected(connected) {

    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#messages").html("");
}


var socket = new SockJS('/gameof3-websocket');
stompClient = Stomp.over(socket);
stompClient.connect({}, function (frame) {
    setConnected(true);
    console.log('Connected: ' + frame);
    stompClient.subscribe('/topic/messages', function (message) {
        showMessages(JSON.parse(message.body).content);
        if(playerIndex == null){
            playerIndex = JSON.parse(message.body).playerIndex;
            console.log('player ' + playerIndex + ' is ready');
        }
    });
    stompClient.send("/app/join", {});
});
    

function start() {
	if(playerIndex == null){
		alert('First you must register');
	} else {
		stompClient.send("/app/start", {}, JSON.stringify({'playerIndex':playerIndex}));
	}
}

function play() {
	if(playerIndex == null){
		alert('First you must register');
	} else {
		console.log('player ' + playerIndex + ' has send adjustment');
	    stompClient.send("/app/play", {}, JSON.stringify({'playerIndex':playerIndex, 'adjustment': $("#adjustment").val()}));
	}
}

function showMessages(message) {
    $("#messages").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#start" ).click(function() { start(); });
    $( "#play" ).click(function() { play(); });
});


GameOf3 is enjoyable game by Cemil Aydoğdu
https://bitbucket.org/caydogdu/gameof3

run options

1- Running as a packaged application
If you use the Spring Boot Maven or Gradle plugins to create an executable jar you can run your application using java -jar. For example:
$ java -jar target/gameof3-0.0.1-SNAPSHOT.jar
It is also possible to run a packaged application with remote debugging support enabled. This allows you to attach a debugger to your packaged application:

2- Using the Maven plugin
The Spring Boot Maven plugin includes a run goal which can be used to quickly compile and run your application. Applications run in an exploded form just like in your IDE.

$ mvn spring-boot:run

to play open type localhost:8081 on any browser 